<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class AfupController extends AbstractController
{
    /**
     * @Route("/afup", name="afup")
     */
    public function index(TagAwareCacheInterface $afupCache, ProductRepository $repository)
    {
        $value = $afupCache->get('foo', function ($item) {
            sleep(1);
            $item->expiresAfter(5);

            return mt_rand();
        });

        $item = $afupCache->getItem('product');

        dump($item);

        if (!$item->isHit()) {
            $products = $repository->findProducts();
            $item->set($products);
            $afupCache->save($item);
        } else {
            $products = $item->get();
        }

        return $this->render('home/afup.html.twig', [
            'value' => $value,
            'products' => $products,
        ]);
    }
}
