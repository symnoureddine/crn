#!/bin/sh

mkdir -p ./build && chmod g+ws ./build
mkdir -p /tmp/behat_screenshots && chmod a+rwx -R /tmp/behat_screenshots

chown -R www-data:www-data ./
chown -R root:root /tmp/behat_screenshots

/usr/sbin/apache2ctl -D FOREGROUND
