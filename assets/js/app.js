/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
require('../css/app.scss');

require('bootstrap/dist/css/bootstrap.min.css');

require('tablesorter/dist/css/theme.blue.css');

require('tablesorter/dist/js/jquery.tablesorter.min');
require('../js/table')
require('bootstrap');

require('intl-tel-input/src/js/intlTelInput.js');
require('../js/intlTelInput');
require('../css/intlTelInput.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
//import $ from 'jquery';



