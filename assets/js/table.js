$(function( ) {
    $('#myTable').tablesorter({
        theme : 'blue',
        sortList: [[0,0], [1,0]]
    });

    $('[data-toggle="tooltip"]').tooltip();
});
